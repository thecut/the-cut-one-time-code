The Cut Django App: One Time Code


==================
thecut.onetimecode
==================

To install this application (whilst in the project's activated virtualenv)::
    pip install git+ssh://git@git.thecut.net.au/thecut-onetimecode

This application requires djangorestframework==2.3.13
    pip install djangorestframework==2.3.13

Was tested with django 1.6 and includes south migrations.


Add this app to ``INSTALLED_APPS`` like this:

INSTALLED_APPS = (

    'thecut.onetimecode',

)

And add api urls like this:

urlpatterns = patterns(
    '',

    # One Time Code
    (r'^onetimecode/', include('thecut.onetime.urls.api')),

)
