# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
import random
from datetime import timedelta
from django.contrib.contenttypes import generic
from django.contrib.contenttypes.models import ContentType
from django.db import models, IntegrityError
from django.utils import timezone

MAX_ITERATIONS = 100


class OneTimeCode(models.Model):
    """
    This represents unique one-time code to assist in authorisation tasks.

    Server generates one-time code that is associated with another object in
     database (through generic foreign key) and shows it to the user.

    User uses this one time code to access associated object through other
    means.

    Example A (access from external system):

    Access the 'Customer' object in system A from system B.

    1) Operator of system A creates one time code, associated with database
        object 'Customer Z' and gives it to the customer.
    2) Customer enters this code into the system B.
    3) System B makes an api call to the system A providing this code.
    4) System A returns 'Customer Z' primary key.
    5) System B saves this primary key to make later calls to system A to fetch
        information about database object 'Customer Z'.


    Example B (two-factor authentication):

    1) Customer enters login and password to access Terminal.
    2) If the login and password are correct, Terminal generates code,
        associates it with Customer record in database and sends this code to
        customer's second factor device.
    3) Customer receives code and enters it.
    4) Terminal verifies that this code is associated with this customer and
        grants an access.


    Example C (email verification):

    1) Server associates one time code with email object in database.
    2) Sends this code to this email.
    3) Customer receives email and enters code.
    4) Server checks that code is associated with email.


    """
    code = models.CharField(max_length=20, unique=True, db_index=True,
                            editable=False)
    """`Code` field should not be accessed directly, class methods must be used
    instead."""

    expires_at = models.DateTimeField(blank=True, null=True, editable=False,
                                      db_index=True)
    """Records future date when the code is no longer valid."""

    target_type = models.ForeignKey('contenttypes.ContentType',
                                    related_name='+', editable=False,
                                    null=True, blank=True)
    target_id = models.IntegerField(db_index=True, null=True, blank=True,
                                    editable=False)
    target = generic.GenericForeignKey('target_type', 'target_id')
    """Generic foreign key to the associated object."""

    @classmethod
    def generate(cls, target, code_length=6, expire_minutes=7*24*60,
                 alphabet="0123456789"):
        """
        Generates and saves new OneTimeCode and returns generated code, not the
        object, but code as :py:class:`str`.

        :param :target Django model instance associated with this code.
        :param :code_length length of one-time code.
        :param :expire_minutes for how many minutes this code should be
            considered valid.
        :param :alphabet :py:class:`str` to choose code letters from it.

        :return: :py:class:`str` one-time code.
        """
        if not target:
            raise ValueError("OneTimeCode target must be present.")

        if code_length < 3 or code_length > 20:
            raise ValueError("OneTimeCode code_length must be between 3 and 20")

        if expire_minutes < 1:
            raise ValueError("OneTimeCode expire_minutes must be more than 0")

        cls.clean_expired()

        expires_at = timezone.now() + timedelta(minutes=expire_minutes)
        target_type = ContentType.objects.get_for_model(target,
                                                        for_concrete_model=True)
        for i in range(MAX_ITERATIONS):
            try:
                return cls.objects.create(
                    code=cls._gen_code(code_length, alphabet),
                    expires_at=expires_at, target_type=target_type,
                    target_id=target.pk).code
            except IntegrityError:
                pass

        raise ValueError("Can't find unused one time code for length {} after "
                         "{} iterations".format(code_length, MAX_ITERATIONS))

    @classmethod
    def get_object(cls, code):
        """
        Finds by code associated object, verifies the code is still valid and
            returns associated object.
        Returns `None` if code is bad: expired or not found.

        Can be used only once, it marks the code as expired

        :param code: :py:class:`str` one time code itself.
        :return: associated object if code is valid.
        """
        try:
            one_time_code = cls.objects.get(code=code)
            if not one_time_code.is_valid():
                return None
            one_time_code.expire_now()
            return one_time_code.target
        except cls.DoesNotExist:
            return None
        finally:
            cls.clean_expired()

    def is_valid(self):
        """
        Checks that code has not expired.
        :return: :py:class:`bool`
        """
        return self.expires_at > timezone.now()

    def expire_now(self):
        """
        Marks the code as expired. To be deleted on the next `clean_expired`.
        """
        self.expires_at = timezone.now()
        self.save(update_fields=['expires_at'])
    expire_now.alters_data = True

    @classmethod
    def clean_expired(cls):
        """
        Deletes expired one time codes from database.
        Does not need to be called manually, gets called in `generate` and
            `get_object`.
        """
        now = timezone.now()
        cls.objects.filter(expires_at__lt=now).delete()

    @staticmethod
    def _gen_code(code_length, alphabet):
        sys_random = random.SystemRandom()
        return ''.join(sys_random.choice(alphabet) for _ in range(code_length))
