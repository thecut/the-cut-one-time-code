# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from thecut.onetimecode.models import OneTimeCode
from thecut.onetimecode.permissions import IsActiveUser


@api_view(['GET'])
@permission_classes((IsAuthenticated, IsActiveUser))
def get_object(request, code):
    target_object = OneTimeCode.get_object(code)
    if target_object:
        return Response({
            'valid': True,
            'id': target_object.pk,
            'type': target_object.__class__.__name__
        })
    return Response({
        'valid': False
    })
