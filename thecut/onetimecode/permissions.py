# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from rest_framework import permissions


class IsActiveUser(permissions.BasePermission):
    """ Only Active Users have permission """

    def has_permission(self, request, view):
        return request.user.is_active
