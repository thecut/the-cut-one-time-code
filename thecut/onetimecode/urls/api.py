# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from django.conf.urls import patterns, url, include
from thecut.onetimecode.views import get_object


urls = patterns(
    'thecut.onetimecode',

    url(r'^(?P<code>\w+)/$', get_object, name='get_object'),
)

urlpatterns = patterns('', (r'^', include(urls, namespace='onetimecode')))
